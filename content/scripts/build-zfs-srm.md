+++
title = "build ZFS support for SystemRescue"
+++

## Overview

Support for the ZFS filesystem [cannot be distributed](https://www.system-rescue.org/FAQ/#when-is-systemrescue-going-to-support-zfs) as part of the official SystemRescue ISO, due to [licensing issues](https://sfconservancy.org/blog/2016/feb/25/zfs-and-linux/). However, users can build this themselves in a [reproducible](https://reproducible-builds.org/docs/definition/) manner, with the help of the `build-zfs-srm` script. This script simplifies the process of setting up the build environment, compiling the requisite OpenZFS kernel modules and tools, and producing an [SRM file](https://www.system-rescue.org/Modules/) that can be loaded into a running instance of SystemRescue.

## Preparation

 - Ensure that your system has at least 4 GB of RAM, as OpenZFS will be compiled in memory.
 - Boot SystemRescue.
 - Make sure the system is connected to the Internet. SystemRescue uses and enables DHCP by default, but depending on local network setup you may have to do some tweaks like setting a different default gateway or configuring VLAN tagging.
 - Run `build-zfs-srm` at the shell (see below for details).

## Usage

```
build-zfs-srm [--latest]
```
The script takes only a single option, `--latest`. If specified, it will build the latest/current version of OpenZFS present in the AUR repo. (The default is to use the version that was current when the running instance of SystemRescue was released, for the sake of reproducibility.)

When the build completes, the script will print a summary detailing some aspects (e.g. the location and [SHA-512](https://en.wikipedia.org/wiki/SHA-2) hash) of the resulting SRM file. Due to the reproducible nature of the build, if `--latest` is not given, the built SRM should always be the same for a given release of SystemRescue. (Specifying `--latest` will yield a different SRM as the OpenZFS packages are updated in AUR.)

Save a copy of the SRM file to disk or some other form of persistent media, and reboot.

## Loading the SRM

More information on SRM files and their usage can be found [here](https://www.system-rescue.org/Modules/), but the simplest way of loading one is with a shell command like one of the following:
```
load-srm /path/to/sysrescue-11.00-zfs.srm

load-srm http://www.example.com/path/to/sysrescue-11.00-zfs.srm

load-srm https://secure.example.com/path/to/sysrescue-11.00-zfs.srm
```
Specify the appropriate path or URL to the SRM file. Note that the SRM is only compatible with the version of SystemRescue on which it was built! (For example, you cannot take an SRM that was built on 10.01 and use it on 10.02.)

To have ZFS support loaded automatically at boot time, the SystemRescue [customization script](/scripts/sysrescue-customize/) can produce a custom ISO with the SRM baked in.

## Reference builds

The author of this script maintains a [Web page](https://gitlab.com/-/snippets/3617038) with information about ZFS SRM builds for specific releases of SystemResue. You may want to compare the details of your build to the ones listed there.

## Reporting issues

Please report any issues with the script's functioning at the main SystemRescue [issue tracker](https://gitlab.com/systemrescue/systemrescue-sources/-/issues).
